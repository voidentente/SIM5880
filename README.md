SIM5880
=======

SIM5880 is an educational simulator of PIC16F8X microcontrollers written in Java, with an optional frontend written in Rust.

Supported PIC16F8X models are:
- PIC16F83
- PIC16CR83
- PIC16F84
- PIC16CR84

Project Structure
-----------------

The project is divided into three parts:
- SIM5880.backend, the simulator library
- SIM5880.frontend, bindings to
- SIM5880-rs, the native frontend library

Building
--------

The project can be built with the provided Makefile.
The intended development platform of this project is Linux.

Natives are provided for the following targets:
- `x86_64-unknown-linux-gnu`
- `x86_64-pc-windows-gnu`

License
-------

The SIM5880 project is published under the MIT license.