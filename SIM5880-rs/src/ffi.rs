use crate::State;
use jni::sys::jint;
pub use jni::{objects::JObject, JNIEnv};
use std::{
    panic::catch_unwind,
    sync::atomic::{AtomicBool, Ordering},
};

const STATUS_EXIT: jint = 0;
const STATUS_CONTINUE: jint = 1;
const STATUS_LOCKED: jint = 2;
const STATUS_UNINITIALIZED: jint = 3;
const STATUS_ERROR: jint = 4;
const STATUS_PANIC: jint = 5;

static mut INSTANCE: Option<State> = None;
static LOCK: AtomicBool = AtomicBool::new(false);

#[export_name = "Java_SIM5880_frontend_NativeFrontend_init"]
pub extern "system" fn init<'local>(env: JNIEnv<'local>, obj: JObject<'local>) -> jint {
    let status;

    if LOCK.swap(true, Ordering::Relaxed) {
        // The value of the atomic switched from `true` to `true`.
        // The resource is busy. Nothing further needs to be done.
        status = STATUS_LOCKED;
    } else {
        // The value of the atomic switched from `false` to `true`.
        // Until this is reset, all other calls result in the code block above.
        // Using INSTANCE mutably is now safe, since serial access is enforced.

        match catch_unwind(|| State::init(env, obj)) {
            Ok(Ok(state)) => {
                unsafe { INSTANCE = Some(state) };
                status = STATUS_CONTINUE;
            }
            Ok(Err(_)) => {
                status = STATUS_ERROR;
            }
            Err(_) => {
                status = STATUS_PANIC;
            }
        };

        LOCK.store(false, Ordering::Relaxed);
    }

    status
}

#[export_name = "Java_SIM5880_frontend_NativeFrontend_update"]
pub extern "system" fn update<'local>(mut env: JNIEnv<'local>, obj: JObject<'local>) -> jint {
    let status;

    if LOCK.swap(true, Ordering::Relaxed) {
        // The value of the atomic switched from `true` to `true`.
        // The resource is busy. Nothing further needs to be done.
        status = STATUS_LOCKED;
    } else {
        // The value of the atomic switched from `false` to `true`.
        // Until this is reset, all other calls result in the code block above.
        // Using INSTANCE mutably is now safe, since serial access is enforced.

        if unsafe { INSTANCE.is_none() } {
            status = STATUS_UNINITIALIZED;
        } else {
            match catch_unwind(move || unsafe {
                INSTANCE.as_mut().unwrap_unchecked().update(&mut env, &obj)
            }) {
                Ok(Ok(true)) => {
                    status = STATUS_EXIT;
                }
                Ok(Ok(false)) => {
                    status = STATUS_CONTINUE;
                }
                Ok(Err(_)) => {
                    status = STATUS_ERROR;
                }
                Err(_) => {
                    status = STATUS_PANIC;
                }
            };
        }

        LOCK.store(false, Ordering::Relaxed);
    }

    status
}

#[export_name = "Java_SIM5880_frontend_NativeFrontend_reset"]
pub extern "system" fn reset<'local>(env: JNIEnv<'local>, obj: JObject<'local>) -> jint {
    let status;

    if LOCK.swap(true, Ordering::Relaxed) {
        // The value of the atomic switched from `true` to `true`.
        // The resource is busy. Nothing further needs to be done.
        status = STATUS_LOCKED;
    } else {
        // The value of the atomic switched from `false` to `true`.
        // Until this is reset, all other calls result in the code block above.
        // Using INSTANCE mutably is now safe, since serial access is enforced.

        if unsafe { INSTANCE.is_none() } {
            status = STATUS_UNINITIALIZED;
        } else {
            match catch_unwind(|| unsafe { INSTANCE.as_mut().unwrap_unchecked().reset(env, obj) }) {
                Ok(Ok(())) => {
                    unsafe { INSTANCE = None };
                    status = STATUS_CONTINUE;
                }
                Ok(Err(_)) => {
                    status = STATUS_ERROR;
                }
                Err(_) => {
                    status = STATUS_PANIC;
                }
            }
        }

        LOCK.store(false, Ordering::Relaxed);
    }

    status
}
