use std::time::Instant;

use crate::ffi;
use anyhow::Result;
use crossterm::event::{Event, KeyCode, KeyEvent};
use ratatui::{
    layout::{Constraint, Direction, Layout},
    text::Text,
    widgets::Block,
    Frame,
};

pub struct State {
    start: Instant,
}

impl Default for State {
    fn default() -> Self {
        Self {
            start: Instant::now(),
        }
    }
}

impl State {
    pub fn draw(
        &mut self,
        _env: &mut ffi::JNIEnv,
        _obj: &ffi::JObject,
        frame: &mut Frame,
    ) -> Result<()> {
        let main_layout = Layout::new(
            Direction::Vertical,
            [Constraint::Length(1), Constraint::Fill(1)],
        )
        .split(frame.size());

        let frame_count = frame.count();
        let seconds_since_start = self.start.elapsed().as_secs_f64();
        let frames_per_second = frame_count as f64 / seconds_since_start;

        frame.render_widget(
            Text::raw(format!(
                "{frame_count:08} frames, {seconds_since_start:.01} seconds since start, {frames_per_second:.01} frames per second"
            )),
            main_layout[0],
        );
        frame.render_widget(Block::bordered().title("SIM5880"), main_layout[1]);

        Ok(())
    }

    pub fn handle(
        &mut self,
        event: Event,
        env: &mut ffi::JNIEnv,
        obj: &ffi::JObject,
        exit: &mut bool,
    ) -> Result<()> {
        match event {
            Event::Key(event) => self.handle_key_event(event, env, obj, exit),
            _ => Ok(()),
        }
    }

    fn handle_key_event(
        &mut self,
        event: KeyEvent,
        _env: &mut ffi::JNIEnv,
        _obj: &ffi::JObject,
        exit: &mut bool,
    ) -> Result<()> {
        match event.code {
            KeyCode::Char('Q') => {
                *exit = true;
                Ok(())
            }
            KeyCode::Esc => panic!("uh oh!"),
            _ => Ok(()),
        }
    }
}
