#![allow(non_snake_case)]

mod app;
mod ffi;

use anyhow::Result;
use crossterm::{
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{backend::CrosstermBackend, Terminal};
use std::{io, time::Duration};

pub struct State {
    app: app::State,
    terminal: Terminal<CrosstermBackend<io::Stdout>>,
}

impl State {
    fn init<'local>(_: ffi::JNIEnv<'local>, _: ffi::JObject<'local>) -> Result<Self> {
        enable_raw_mode()?;
        let mut stdout = io::stdout();
        execute!(stdout, EnterAlternateScreen)?;
        let backend = CrosstermBackend::new(stdout);
        let terminal = Terminal::new(backend)?;
        let app = app::State::default();
        Ok(Self { terminal, app })
    }

    fn update<'local>(
        &mut self,
        env: &mut ffi::JNIEnv<'local>,
        obj: &ffi::JObject<'local>,
    ) -> Result<bool> {
        let mut exit = false;

        self.terminal.autoresize()?;
        self.terminal
            .draw(|frame| drop(self.app.draw(env, obj, frame)))?;

        const TIMEOUT: Duration = Duration::from_millis(1);
        if let Ok(true) = crossterm::event::poll(TIMEOUT) {
            let event = crossterm::event::read()?;
            self.app.handle(event, env, obj, &mut exit)?;
        }

        Ok(exit)
    }

    fn reset<'local>(&mut self, _: ffi::JNIEnv<'local>, _: ffi::JObject<'local>) -> Result<()> {
        disable_raw_mode()?;
        execute!(self.terminal.backend_mut(), LeaveAlternateScreen)?;
        self.terminal.show_cursor()?;
        Ok(())
    }
}
