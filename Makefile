default: SIM5880-rs SIM5880
.PHONY: SIM5880-rs SIM5880


SIM5880:
	javac -d target -h headers SIM5880/SIM5880.java
	jar --create --main-class=SIM5880.SIM5880 --file SIM5880.jar -C target .

SIM5880-rs: SIM5880-rs-x86_64-unknown-linux-gnu SIM5880-rs-x86_64-pc-windows-gnu 

SIM5880-rs-x86_64-unknown-linux-gnu:
	cd SIM5880-rs; cargo build --release --target x86_64-unknown-linux-gnu
	mkdir -p target/SIM5880-rs/x86_64-unknown-linux-gnu
	cp SIM5880-rs/target/x86_64-unknown-linux-gnu/release/libSIM5880_rs.so target/SIM5880-rs/x86_64-unknown-linux-gnu/

SIM5880-rs-x86_64-pc-windows-gnu:
	cd SIM5880-rs; cargo build --release --target x86_64-pc-windows-gnu
	mkdir -p target/SIM5880-rs/x86_64-pc-windows-gnu
	cp SIM5880-rs/target/x86_64-pc-windows-gnu/release/SIM5880_rs.dll target/SIM5880-rs/x86_64-pc-windows-gnu/


run:
	java -jar SIM5880.jar