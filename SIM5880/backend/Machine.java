package SIM5880.backend;

/**
 * Represents the entire machine state.
 * Can be run separately with an {@link Executor}.
 */
public class Machine extends MachineBase implements Instructions {

    public enum Model {
        PIC16F83(512, 36, 64),
        PIC16CR83(512, 36, 64),
        PIC16F84(1024, 68, 64),
        PIC16CR84(1024, 68, 64);

        private final int programLen, registerLen, diskLen;

        Model(int programLen, int registerLen, int diskLen) {
            this.programLen = programLen;
            this.registerLen = registerLen;
            this.diskLen = diskLen;
        }
    }

    //////////////////////////////////////////
    ////////////// CONSTRUCTORS //////////////
    //////////////////////////////////////////

    /**
     * Create machine with model parameters.
     */
    public Machine(Model model) {
        this(model.programLen, model.registerLen, model.diskLen);
    }

    /**
     * Create machine with custom parameters.
     */
    public Machine(int programLen, int fileRegisterLen, int diskLen) {
        this.program = new short[programLen];
        this.fileRegister = new byte[fileRegisterLen];
        this.disk = new byte[diskLen];
    }

    //////////////////////////////////////////
    ////////////// INSTRUCTIONS //////////////
    //////////////////////////////////////////

    @Override
    public void ADDWF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'ADDWF'");
    }

    @Override
    public void ANDWF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'ANDWF'");
    }

    @Override
    public void CLRF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'CLRF'");
    }

    @Override
    public void CLRW() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'CLRW'");
    }

    @Override
    public void COMF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'COMF'");
    }

    @Override
    public void DECF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'DECF'");
    }

    @Override
    public void DECFSZ() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'DECFSZ'");
    }

    @Override
    public void INCF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'INCF'");
    }

    @Override
    public void INCFSZ() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'INCFSZ'");
    }

    @Override
    public void IORWF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'IORWF'");
    }

    @Override
    public void MOVF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'MOVF'");
    }

    @Override
    public void MOVWF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'MOVWF'");
    }

    @Override
    public void NOP() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'NOP'");
    }

    @Override
    public void RLF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'RLF'");
    }

    @Override
    public void RRF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'RRF'");
    }

    @Override
    public void SUBWF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'SUBWF'");
    }

    @Override
    public void SWAPF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'SWAPF'");
    }

    @Override
    public void XORWF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'XORWF'");
    }

    @Override
    public void BCF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'BCF'");
    }

    @Override
    public void BSF() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'BSF'");
    }

    @Override
    public void BTFSC() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'BTFSC'");
    }

    @Override
    public void BTFSS() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'BTFSS'");
    }

    @Override
    public void ADDLW() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'ADDLW'");
    }

    @Override
    public void ANDLW() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'ANDLW'");
    }

    @Override
    public void CALL() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'CALL'");
    }

    @Override
    public void CLRWDT() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'CLRWDT'");
    }

    @Override
    public void GOTO() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'GOTO'");
    }

    @Override
    public void IORLW() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'IORLW'");
    }

    @Override
    public void MOVLW() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'MOVLW'");
    }

    @Override
    public void RETFIE() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'RETFIE'");
    }

    @Override
    public void RETLW() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'RETLW'");
    }

    @Override
    public void RETURN() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'RETURN'");
    }

    @Override
    public void SLEEP() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'SLEEP'");
    }

    @Override
    public void SUBLW() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'SUBLW'");
    }

    @Override
    public void XORLW() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'XORLW'");
    }

}