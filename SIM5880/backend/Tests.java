package SIM5880.backend;

public class Tests {

        public static final char[] TPicSim1 = {
                        0x3011, // 0x0000 movlw 0x11
                        0x3930, // 0x0001 andlw 0x30
                        0x380D, // 0x0002 iorlw 0x0D
                        0x3C3D, // 0x0003 sublw 0x3D
                        0x3A20, // 0x0004 xorlw 0x20
                        0x3E25, // 0x0005 addlw 0x25
                        0x2806, // 0x0006 goto 0x0006
        };

        public static final char[] TPicSim2 = {
                        0x3011, // 0x0000 movlw 0x11
                        0x2006, // 0x0001 call 0x0006
                        0x0000, // 0x0002 nop
                        0x2008, // 0x0003 call 0x0008
                        0x0000, // 0x0004 nop
                        0x2800, // 0x0005 goto 0x0000
                        0x3E25, // 0x0006 addlw 0x25
                        0x0008, // 0x0007 return
                        0x3477, // 0x0008 retlw 0x77
                        0x2809, // 0x0009 goto 0x0009
        };

        public static final char[] TPicSim3 = {
                        0x3011, // 0x0000 movlw 0x11
                        0x008C, // 0x0001 movwf 0x0C
                        0x3014, // 0x0002 movlw 0x14
                        0x070C, // 0x0003 addwf 0x0C,w
                        0x078C, // 0x0004 addwf 0x0C
                        0x050C, // 0x0005 andwf 0x0C,w
                        0x008D, // 0x0006 movwf 0x0D
                        0x018C, // 0x0007 clrf 0x0C
                        0x090D, // 0x0008 comf 0x0D,w
                        0x030C, // 0x0009 decf 0x0C,w
                        0x0A8D, // 0x000A incf 0x0D
                        0x088C, // 0x000B movf 0x0C
                        0x048C, // 0x000C iorwf 0x0C
                        0x020D, // 0x000D subwf 0x0D,w
                        0x0E8D, // 0x000E swapf 0x0D
                        0x068C, // 0x000F xorwf 0x0C
                        0x0100, // 0x0010 clrw
                        0x020C, // 0x0011 subwf 0x0C,w
                        0x020D, // 0x0012 subwf 0x0D,w
                        0x028D, // 0x0013 subwf 0x0D
                        0x028D, // 0x0014 subwf 0x0D
                        0x2815, // 0x0015 goto 0x0015
        };

        public static final char[] TPicSim4 = {
                        0x3011, // 0x0000 movlw 0x11
                        0x008C, // 0x0001 movwf 0x0C
                        0x3E11, // 0x0002 addlw 0x11
                        0x0D8C, // 0x0003 rlf 0x0C
                        0x0D8C, // 0x0004 rlf 0x0C
                        0x0D8C, // 0x0005 rlf 0x0C
                        0x0D0C, // 0x0006 rlf 0x0C,w
                        0x0D8C, // 0x0007 rlf 0x0C
                        0x0D0C, // 0x0008 rlf 0x0C,w
                        0x0C8C, // 0x0009 rlf 0x0C
                        0x008D, // 0x000A movwf 0x0D
                        0x0C8D, // 0x000B rrf 0x0D
                        0x0C0D, // 0x000C rrf 0x0D,w
                        0x3009, // 0x000D movlw 0x09
                        0x008C, // 0x000E movwf 0x0C
                        0x0100, // 0x000F clrw
                        0x3E01, // 0x0010 addlw 0x01
                        0x078D, // 0x0011 addwf 0x0D
                        0x0B8C, // 0x0012 decfsz 0x0C
                        0x2810, // 0x0013 goto 0x0010
                        0x30F0, // 0x0014 movlw 0xF0
                        0x008C, // 0x0015 movwf 0x0C
                        0x018D, // 0x0016 clrf 0x0D
                        0x0100, // 0x0017 clrw
                        0x070C, // 0x0018 addwf 0x0C,w
                        0x0A8D, // 0x0019 incf 0x0D
                        0x0F8C, // 0x001A incfsz 0x0C
                        0x2818, // 0x001B goto 0x0018
                        0x281C, // 0x001C goto 0x001C
        };

        public static final char[] TPicSim5 = {
                        0x3011, // 0x0000 movlw 0x11
                        0x008C, // 0x0001 movwf 0x0C
                        0x018D, // 0x0002 clrf 0x0D
                        0x178C, // 0x0003 bsf 0x0C,7
                        0x158C, // 0x0004 bsf 0x0C,3
                        0x120C, // 0x0005 bcf 0x0C,4
                        0x100C, // 0x0006 bcf 0x0C,0
                        0x180C, // 0x0007 btfsc 0x0C,0x00
                        0x0A8D, // 0x0008 incf 0x0D
                        0x0A8D, // 0x0009 incf 0x0D
                        0x198C, // 0x000A btfsc 0x0C,0x03
                        0x0A8D, // 0x000B incf 0x0D
                        0x0A8D, // 0x000C incf 0x0D
                        0x1D0C, // 0x000D btfss 0x0C,0x02
                        0x0A8D, // 0x000E incf 0x0D
                        0x0A8D, // 0x000F incf 0x0D
                        0x1F8C, // 0x0010 btfss 0x0C,0x07
                        0x0A8D, // 0x0011 incf 0x0D
                        0x038D, // 0x0012 incf 0x0D
                        0x2813, // 0x0013 goto 0x0013
        };

        public static final char[] TPicSim6 = {
                        0x3020, // 0x0000 movlw 0x20
                        0x008C, // 0x0001 movwf 0x0C
                        0x3010, // 0x0002 movlw 0x10
                        0x0084, // 0x0003 movwf 0x04
                        0x008D, // 0x0004 movwf 0x0D
                        0x080C, // 0x0005 movf 0x0C,w
                        0x0080, // 0x0006 movwf 0x00
                        0x3E01, // 0x0007 addlw 0x01
                        0x0A84, // 0x0008 incf 0x04
                        0x0B8D, // 0x0009 decfsz 0x0D
                        0x2806, // 0x000A goto 0x0006
                        0x301F, // 0x000B movlw 0x1F
                        0x0084, // 0x000C movwf 0x04
                        0x30F0, // 0x000D movlw 0xF0
                        0x008D, // 0x000E movwf 0x0D
                        0x0100, // 0x000F clrw
                        0x0700, // 0x0010 addwf 0x00,w
                        0x0384, // 0x0011 decf 0x04
                        0x0F8D, // 0x0012 incfsz 0x0D
                        0x2810, // 0x0013 goto 0x0010
                        0x008D, // 0x0014 movwf 0x0D
                        0x0A84, // 0x0015 incf 0x04
                        0x0C80, // 0x0016 rrf 0x00
                        0x0A80, // 0x0017 incf 0x00
                        0x0C80, // 0x0018 rrf 0x00
                        0x1780, // 0x0019 bsf 0x00,0x07
                        0x1003, // 0x001A bcf 0x03,0x00
                        0x0D80, // 0x001B rlf 0x00
                        0x0A84, // 0x001C incf 0x04
                        0x0D80, // 0x001D rlf 0x00
                        0x0E80, // 0x001E swapf 0x00
                        0x0680, // 0x001F xorwf 0x00
                        0x1A80, // 0x0020 btfsc 0x00,0x05
                        0x2800, // 0x0021 goto 0x0000
                        0x1D00, // 0x0022 btfss 0x00,0x02
                        0x2800, // 0x0023 goto 0x0000
                        0x1980, // 0x0024 btfsc 0x00,0x03
                        0x2827, // 0x0025 goto 0x0027
                        0x2800, // 0x0026 goto 0x0000
                        0x2827, // 0x0027 goto 0x0027
        };

        public static final char[] TPicSim7 = {
                        0x3001, // 0x0000 movlw 0b00000001
                        0x1683, // 0x0001 bsf 0x03,0x05
                        0x0081, // 0x0002 movwf 0x01
                        0x1283, // 0x0003 bcf 0x03,0x05
                        0x3001, // 0x0004 movlw 0x01
                        0x0081, // 0x0005 movwf 0x01
                        0x0190, // 0x0006 clrf 0x10
                        0x0000, // 0x0007 nop
                        0x0000, // 0x0008 nop
                        0x0000, // 0x0009 nop
                        0x0A90, // 0x000A incf 0x10
                        0x0801, // 0x000B movf 0x01,w
                        0x1D03, // 0x000C btfss 0x03,0x02
                        0x2807, // 0x000D goto 0x0007
                        0x3003, // 0x000E movlw 0b00000011
                        0x1683, // 0x000F bsf 0x03,0x05
                        0x0081, // 0x0010 movwf 0x01
                        0x1283, // 0x0011 bcf 0x03,0x05
                        0x3001, // 0x0012 movlw 0x01
                        0x0081, // 0x0013 movwf 0x01
                        0x0190, // 0x0014 clrf 0x10
                        0x0A90, // 0x0015 incf 0x10
                        0x0801, // 0x0016 movf 0x01,w
                        0x1D03, // 0x0017 btfss 0x03,0x02
                        0x2815, // 0x0018 goto 0x0015
                        0x3038, // 0x0019 movlw 0b00111000
                        0x1683, // 0x001A bsf 0x03,0x05
                        0x0081, // 0x001B movwf 0x01
                        0x1283, // 0x001C bcf 0x03,0x05
                        0x0181, // 0x001D clrf 0x01
                        0x1E01, // 0x001E btfss 0x01,0x04
                        0x281E, // 0x001F goto 0x001E
                        0x3031, // 0x0020 movlw 0b00110001
                        0x1683, // 0x0021 bsf 0x03,0x05
                        0x0081, // 0x0022 movwf 0x01
                        0x1283, // 0x0023 bcf 0x03,0x05
                        0x0181, // 0x0024 clrf 0x01
                        0x1D81, // 0x0025 btfss 0x01,0x03
                        0x2825, // 0x0026 goto 0x0025
                        0x2827, // 0x0027 goto 0x0027
        };

        public static final char[] TPicSim8 = {
                        0x281C, // 0x0000 goto 0x001C
                        0x0000, // 0x0001 nop
                        0x0000, // 0x0002 nop
                        0x0000, // 0x0003 nop
                        0x1D0B, // 0x0004 btfss 0x0B,0x02
                        0x280B, // 0x0005 goto 0x000B
                        0x3054, // 0x0006 movlw 0x54
                        0x00A0, // 0x0007 movwf 0x20
                        0x128B, // 0x0008 bcf 0x0B,0x05
                        0x110B, // 0x0009 bcf 0x0B,0x02
                        0x281B, // 0x000A goto 0x001B
                        0x1C8B, // 0x000B btfss 0x0B,0x01
                        0x2812, // 0x000C goto 0x0012
                        0x3049, // 0x000D movlw 'I'
                        0x00A1, // 0x000E movwf 0x21
                        0x120B, // 0x000F bcf 0x0B,0x04
                        0x108B, // 0x0010 bcf 0x0B,0x01
                        0x281B, // 0x0011 goto 0x001B
                        0x1C0B, // 0x0012 btfss 0x0B,0x00
                        0x2819, // 0x0013 goto 0x0019
                        0x3052, // 0x0014 movlw 'R'
                        0x00A2, // 0x0015 movwf 0x22
                        0x118B, // 0x0016 bcf 0x0B,0x03
                        0x100B, // 0x0017 bcf 0x0B,0x00
                        0x281B, // 0x0018 goto 0x001B
                        0x3046, // 0x0019 movlw 'F'
                        0x00A3, // 0x001A movwf 0x23
                        0x0009, // 0x001B retfie
                        0x3001, // 0x001C movlw 0b00000001
                        0x1683, // 0x001D bcf 0x03,0x05
                        0x0081, // 0x001E movwf 0x01
                        0x1283, // 0x001F bcf 0x03,0x05
                        0x3020, // 0x0020 movlw 0x20
                        0x008B, // 0x0021 movwf 0x0B
                        0x178B, // 0x0022 bsf 0x0B,0x07
                        0x1A8B, // 0x0023 btfsc 0x0B,0x05
                        0x2823, // 0x0024 goto 0x0023
                        0x300F, // 0x0025 movlw 0b00001111
                        0x1683, // 0x0026 bsf 0x03,0x05
                        0x0081, // 0x0027 movwf 0x01
                        0x1283, // 0x0028 bcf 0x03,0x05
                        0x3010, // 0x0029 movlw 0x10
                        0x008B, // 0x002A movwf 0x0B
                        0x178B, // 0x002B bsf 0x0B,0x07
                        0x1A0B, // 0x002C btfsc 0x0B,0x04
                        0x282C, // 0x002D goto 0x002C
                        0x3008, // 0x002E movlw 0x08
                        0x008B, // 0x002F movwf 0x0B
                        0x178B, // 0x0030 bsf 0x0B,0x07
                        0x198B, // 0x0031 btfsc 0x0B,0x03
                        0x2831, // 0x0032 goto 0x0031
                        0x308F, // 0x0033 movlw 0b10001111
                        0x1683, // 0x0034 bsf 0x03,0x05
                        0x0086, // 0x0035 movwf 0x06
                        0x1283, // 0x0036 bcf 0x03,0x05
                        0x3008, // 0x0037 movlw 0x08
                        0x008B, // 0x0038 movwf 0x0B
                        0x178B, // 0x0039 bsf 0x0B,0x07
                        0x198B, // 0x003A btfsc 0x0B,0x03
                        0x283A, // 0x003B goto 0x003A
                        0x283C, // 0x003C goto 0x003C
        };

        public static final char[] TPicSim9 = {
                        0x018C, // 0x0000 clrf 0x0C
                        0x0E03, // 0x0001 swapf 0x03,w
                        0x0090, // 0x0002 movwf 0x10
                        0x0063, // 0x0003 sleep
                        0x0E03, // 0x0004 swapf 0x03,w
                        0x0091, // 0x0005 movwf 0x11
                        0x2806, // 0x0006 goto 0x0006
        };

        public static final char[] TPicSim10 = {
                        0x3000, // 0x0000 movlw 0x00
                        0x008F, // 0x0001 movwf 0x0F
                        0x3005, // 0x0002 movlw 0x05
                        0x008E, // 0x0003 movwf 0x0E
                        0x3010, // 0x0004 movlw 0x10
                        0x0084, // 0x0005 movwf 0x04
                        0x3001, // 0x0006 movlw 0x01
                        0x008A, // 0x0007 movwf 0x0A
                        0x080F, // 0x0008 movf 0x0F,w
                        0x2109, // 0x0009 call 0x0109
                        0x0080, // 0x000A movwf 0x00
                        0x0A84, // 0x000B incf 0x04
                        0x0A8F, // 0x000C incfr 0x0F
                        0x0B8E, // 0x000D decfsz 0x0E
                        0x2806, // 0x000E goto 0x0006
                        0x018A, // 0x000F clrf 0x0A
                        0x080F, // 0x0010 movf 0x0F,w
                        0x2109, // 0x0011 call 0x0109
                        0x2812, // 0x0012 goto 0x0012
                        0x0000, // 0x0013 nop
                        0x0000, // 0x0014 nop
                        0x0000, // 0x0015 nop
                        0x0000, // 0x0016 nop
                        0x0000, // 0x0017 nop
                        0x0000, // 0x0018 nop
                        0x0000, // 0x0019 nop
                        0x0000, // 0x001A nop
                        0x0000, // 0x001B nop
                        0x0000, // 0x001C nop
                        0x0000, // 0x001D nop
                        0x0000, // 0x001E nop
                        0x0000, // 0x001F nop
                        0x0000, // 0x0020 nop
                        0x0000, // 0x0021 nop
                        0x0000, // 0x0022 nop
                        0x0000, // 0x0023 nop
                        0x0000, // 0x0024 nop
                        0x0000, // 0x0025 nop
                        0x0000, // 0x0026 nop
                        0x0000, // 0x0027 nop
                        0x0000, // 0x0028 nop
                        0x0000, // 0x0029 nop
                        0x0000, // 0x002A nop
                        0x0000, // 0x002B nop
                        0x0000, // 0x002C nop
                        0x0000, // 0x002D nop
                        0x0000, // 0x002E nop
                        0x0000, // 0x002F nop
                        0x0000, // 0x0030 nop
                        0x0000, // 0x0031 nop
                        0x0000, // 0x0032 nop
                        0x0000, // 0x0033 nop
                        0x0000, // 0x0034 nop
                        0x0000, // 0x0035 nop
                        0x0000, // 0x0036 nop
                        0x0000, // 0x0037 nop
                        0x0000, // 0x0038 nop
                        0x0000, // 0x0039 nop
                        0x0000, // 0x003A nop
                        0x0000, // 0x003B nop
                        0x0000, // 0x003C nop
                        0x0000, // 0x003D nop
                        0x0000, // 0x003E nop
                        0x0000, // 0x003F nop
                        0x0000, // 0x0040 nop
                        0x0000, // 0x0041 nop
                        0x0000, // 0x0042 nop
                        0x0000, // 0x0043 nop
                        0x0000, // 0x0044 nop
                        0x0000, // 0x0045 nop
                        0x0000, // 0x0046 nop
                        0x0000, // 0x0047 nop
                        0x0000, // 0x0048 nop
                        0x0000, // 0x0049 nop
                        0x0000, // 0x004A nop
                        0x0000, // 0x004B nop
                        0x0000, // 0x004C nop
                        0x0000, // 0x004D nop
                        0x0000, // 0x004E nop
                        0x0000, // 0x004F nop
                        0x0000, // 0x0050 nop
                        0x0000, // 0x0051 nop
                        0x0000, // 0x0052 nop
                        0x0000, // 0x0053 nop
                        0x0000, // 0x0054 nop
                        0x0000, // 0x0055 nop
                        0x0000, // 0x0056 nop
                        0x0000, // 0x0057 nop
                        0x0000, // 0x0058 nop
                        0x0000, // 0x0059 nop
                        0x0000, // 0x005A nop
                        0x0000, // 0x005B nop
                        0x0000, // 0x005C nop
                        0x0000, // 0x005D nop
                        0x0000, // 0x005E nop
                        0x0000, // 0x005F nop
                        0x0000, // 0x0060 nop
                        0x0000, // 0x0061 nop
                        0x0000, // 0x0062 nop
                        0x0000, // 0x0063 nop
                        0x0000, // 0x0064 nop
                        0x0000, // 0x0065 nop
                        0x0000, // 0x0066 nop
                        0x0000, // 0x0067 nop
                        0x0000, // 0x0068 nop
                        0x0000, // 0x0069 nop
                        0x0000, // 0x006A nop
                        0x0000, // 0x006B nop
                        0x0000, // 0x006C nop
                        0x0000, // 0x006D nop
                        0x0000, // 0x006E nop
                        0x0000, // 0x006F nop
                        0x0000, // 0x0070 nop
                        0x0000, // 0x0071 nop
                        0x0000, // 0x0072 nop
                        0x0000, // 0x0073 nop
                        0x0000, // 0x0074 nop
                        0x0000, // 0x0075 nop
                        0x0000, // 0x0076 nop
                        0x0000, // 0x0077 nop
                        0x0000, // 0x0078 nop
                        0x0000, // 0x0079 nop
                        0x0000, // 0x007A nop
                        0x0000, // 0x007B nop
                        0x0000, // 0x007C nop
                        0x0000, // 0x007D nop
                        0x0000, // 0x007E nop
                        0x0000, // 0x007F nop
                        0x0000, // 0x0080 nop
                        0x0000, // 0x0081 nop
                        0x0000, // 0x0082 nop
                        0x0000, // 0x0083 nop
                        0x0000, // 0x0084 nop
                        0x0000, // 0x0085 nop
                        0x0000, // 0x0086 nop
                        0x0000, // 0x0087 nop
                        0x0000, // 0x0088 nop
                        0x0000, // 0x0089 nop
                        0x0000, // 0x008A nop
                        0x0000, // 0x008B nop
                        0x0000, // 0x008C nop
                        0x0000, // 0x008D nop
                        0x0000, // 0x008E nop
                        0x0000, // 0x008F nop
                        0x0000, // 0x0090 nop
                        0x0000, // 0x0091 nop
                        0x0000, // 0x0092 nop
                        0x0000, // 0x0093 nop
                        0x0000, // 0x0094 nop
                        0x0000, // 0x0095 nop
                        0x0000, // 0x0096 nop
                        0x0000, // 0x0097 nop
                        0x0000, // 0x0098 nop
                        0x0000, // 0x0099 nop
                        0x0000, // 0x009A nop
                        0x0000, // 0x009B nop
                        0x0000, // 0x009C nop
                        0x0000, // 0x009D nop
                        0x0000, // 0x009E nop
                        0x0000, // 0x009F nop
                        0x0000, // 0x00A0 nop
                        0x0000, // 0x00A1 nop
                        0x0000, // 0x00A2 nop
                        0x0000, // 0x00A3 nop
                        0x0000, // 0x00A4 nop
                        0x0000, // 0x00A5 nop
                        0x0000, // 0x00A6 nop
                        0x0000, // 0x00A7 nop
                        0x0000, // 0x00A8 nop
                        0x0000, // 0x00A9 nop
                        0x0000, // 0x00AA nop
                        0x0000, // 0x00AB nop
                        0x0000, // 0x00AC nop
                        0x0000, // 0x00AD nop
                        0x0000, // 0x00AE nop
                        0x0000, // 0x00AF nop
                        0x0000, // 0x00B0 nop
                        0x0000, // 0x00B1 nop
                        0x0000, // 0x00B2 nop
                        0x0000, // 0x00B3 nop
                        0x0000, // 0x00B4 nop
                        0x0000, // 0x00B5 nop
                        0x0000, // 0x00B6 nop
                        0x0000, // 0x00B7 nop
                        0x0000, // 0x00B8 nop
                        0x0000, // 0x00B9 nop
                        0x0000, // 0x00BA nop
                        0x0000, // 0x00BB nop
                        0x0000, // 0x00BC nop
                        0x0000, // 0x00BD nop
                        0x0000, // 0x00BE nop
                        0x0000, // 0x00BF nop
                        0x0000, // 0x00C0 nop
                        0x0000, // 0x00C1 nop
                        0x0000, // 0x00C2 nop
                        0x0000, // 0x00C3 nop
                        0x0000, // 0x00C4 nop
                        0x0000, // 0x00C5 nop
                        0x0000, // 0x00C6 nop
                        0x0000, // 0x00C7 nop
                        0x0000, // 0x00C8 nop
                        0x0000, // 0x00C9 nop
                        0x0000, // 0x00CA nop
                        0x0000, // 0x00CB nop
                        0x0000, // 0x00CC nop
                        0x0000, // 0x00CD nop
                        0x0000, // 0x00CE nop
                        0x0000, // 0x00CF nop
                        0x0000, // 0x00D0 nop
                        0x0000, // 0x00D1 nop
                        0x0000, // 0x00D2 nop
                        0x0000, // 0x00D3 nop
                        0x0000, // 0x00D4 nop
                        0x0000, // 0x00D5 nop
                        0x0000, // 0x00D6 nop
                        0x0000, // 0x00D7 nop
                        0x0000, // 0x00D8 nop
                        0x0000, // 0x00D9 nop
                        0x0000, // 0x00DA nop
                        0x0000, // 0x00DB nop
                        0x0000, // 0x00DC nop
                        0x0000, // 0x00DD nop
                        0x0000, // 0x00DE nop
                        0x0000, // 0x00DF nop
                        0x0000, // 0x00E0 nop
                        0x0000, // 0x00E1 nop
                        0x0000, // 0x00E2 nop
                        0x0000, // 0x00E3 nop
                        0x0000, // 0x00E4 nop
                        0x0000, // 0x00E5 nop
                        0x0000, // 0x00E6 nop
                        0x0000, // 0x00E7 nop
                        0x0000, // 0x00E8 nop
                        0x0000, // 0x00E9 nop
                        0x0000, // 0x00EA nop
                        0x0000, // 0x00EB nop
                        0x0000, // 0x00EC nop
                        0x0000, // 0x00ED nop
                        0x0000, // 0x00EE nop
                        0x0000, // 0x00EF nop
                        0x0000, // 0x00F0 nop
                        0x0000, // 0x00F1 nop
                        0x0000, // 0x00F2 nop
                        0x0000, // 0x00F3 nop
                        0x0000, // 0x00F4 nop
                        0x0000, // 0x00F5 nop
                        0x0000, // 0x00F6 nop
                        0x0000, // 0x00F7 nop
                        0x0000, // 0x00F8 nop
                        0x0000, // 0x00F9 nop
                        0x0000, // 0x00FA nop
                        0x0000, // 0x00FB nop
                        0x0000, // 0x00FC nop
                        0x0000, // 0x00FD nop
                        0x0000, // 0x00FE nop
                        0x0000, // 0x00FF nop
                        0x0000, // 0x0100 nop
                        0x0000, // 0x0101 nop
                        0x0000, // 0x0102 nop
                        0x0000, // 0x0103 nop
                        0x0000, // 0x0104 nop
                        0x0000, // 0x0105 nop
                        0x0000, // 0x0106 nop
                        0x0000, // 0x0107 nop
                        0x0000, // 0x0108 nop
                        0x0782, // 0x0109 addwf 0x02
                        0x3460, // 0x010A retlw 0x60
                        0x3461, // 0x010B retlw 0x61
                        0x3462, // 0x010C retlw 0x62
                        0x3463, // 0x010D retlw 0x63
                        0x3464, // 0x010E retlw 0x64
                        0x3465, // 0x010F retlw 0x65
                        0x3466, // 0x0110 retlw 0x66
                        0x3467, // 0x0111 retlw 0x67
                        0x3468, // 0x0112 retlw 0x68
                        0x3469, // 0x0113 retlw 0x69
                        0x346A, // 0x0114 retlw 0x6A
        };

        public static char[] TPicSim11 = {
                        0x3000, // 0x0000 movlw 0b00000000
                        0x1683, // 0x0001 bsf 0x03,0x05
                        0x0081, // 0x0002 movwf 0x01
                        0x1283, // 0x0003 bsf 0x03,0x05
                        0x01A0, // 0x0004 clrf 0x20
                        0x01A1, // 0x0005 clrf 0x21
                        0x01A2, // 0x0006 clrf 0x22
                        0x01A3, // 0x0007 clrf 0x23
                        0x0FA0, // 0x0008 incfsz 0x20
                        0x2808, // 0x0009 goto 0x0008
                        0x0FA1, // 0x000A incfsz 0x21
                        0x2808, // 0x000B goto 0x0008
                        0x0FA2, // 0x000C incfsz 0x22
                        0x2808, // 0x000D goto 0x0008
                        0x0FA3, // 0x000E incfsz 0x23
                        0x2808, // 0x000F goto 0x0008
                        0x2810, // 0x0010 goto 0x0010
        };

        public static char[] TPicSim12 = {
                        0x3000, // 0x0000 movlw 0x00
                        0x008C, // 0x0001 movwf 0x0C
                        0x080C, // 0x0002 movf 0x0C,w
                        0x0089, // 0x0003 movwf 0x09
                        0x3AFF, // 0x0004 xorlw 0xFF
                        0x0088, // 0x0005 movwf 0x08
                        0x1683, // 0x0006 bsf 0x03,0x05
                        0x1208, // 0x0007 bcf 0x08,0x04
                        0x1508, // 0x0008 bsf 0x08,0x02
                        0x3055, // 0x0009 movlw 0x55
                        0x0089, // 0x000A movwf 0x09
                        0x30AA, // 0x000B movlw 0xAA
                        0x0089, // 0x000C movwf 0x09
                        0x1488, // 0x000D bsf 0x08,0x01
                        0x1E08, // 0x000E btfss 0x08,0x04
                        0x280E, // 0x000F goto 0x000E
                        0x1108, // 0x0010 bcf 0x08,0x02
                        0x1283, // 0x0011 bcf 0x03,0x05
                        0x0A8C, // 0x0012 incf 0x0C
                        0x080C, // 0x0013 movf 0x0C,w
                        0x3C40, // 0x0014 sublw 0x40
                        0x1D03, // 0x0015 btfss 0x03,0x02
                        0x2802, // 0x0016 goto 0x0002
                        0x018C, // 0x0017 clrf 0x0C
                        0x080C, // 0x0018 movf 0x0C,w
                        0x0089, // 0x0019 movwf 0x09
                        0x1683, // 0x001A bsf 0x03,0x05
                        0x1408, // 0x001B bsf 0x08,0x00
                        0x1283, // 0x001C bcf 0x03,0x05
                        0x0808, // 0x001D movf 0x08,w
                        0x0A8C, // 0x001E incf 0x0C
                        0x080C, // 0x001F movf 0x0C,w
                        0x3C40, // 0x0020 sublw 0x40
                        0x1D03, // 0x0021 btfss 0x03,0x02
                        0x2818, // 0x0022 goto 0x0018
                        0x2823, // 0x0023 goto 0x0023
        };

        public static final char[] TPicSim13 = {
                        0x1683, // 0x0000 bsf 0x03,0x05
                        0x0186, // 0x0001 clrf 0x06
                        0x1283, // 0x0002 bcf 0x03,0x05
                        0x0186, // 0x0003 clrf 0x06
                        0x1403, // 0x0004 bsf 0x03,0x00
                        0x1C05, // 0x0005 btfss 0x05,0x00
                        0x280B, // 0x0006 goto 0x000B
                        0x0C86, // 0x0007 rrf 0x06
                        0x1803, // 0x0008 btfsc 0x03,0x00
                        0x0C86, // 0x0009 rrf 0x06
                        0x2805, // 0x000A goto 0x0005
                        0x0D86, // 0x000B rlf 0x06
                        0x1803, // 0x000C btfsc 0x03,0x00
                        0x0D86, // 0x000D rlf 0x06
                        0x2805, // 0x000E goto 0x0005
                        0x280F, // 0x000F goto 0x000F
        };

        public static final char[] TPicSim14 = {
                        0x1683, // 0x0000 bsf 0x03,0x05
                        0x0186, // 0x0001 clrf 0x06
                        0x1283, // 0x0002 bcf 0x03,0x05
                        0x0186, // 0x0003 clrf 0x06
                        0x1403, // 0x0004 bsf 0x03,0x00
                        0x1C05, // 0x0005 btfss 0x05,0x00
                        0x280B, // 0x0006 goto 0x000B
                        0x0C86, // 0x0007 rrf 0x06
                        0x1803, // 0x0008 btfsc 0x03,0x00
                        0x0186, // 0x0009 clrf 0x06
                        0x2804, // 0x000A goto 0x0004
                        0x0D86, // 0x000B rlf 0x06
                        0x1803, // 0x000C btfsc 0x03,0x00
                        0x0186, // 0x000D clrf 0x06
                        0x2804, // 0x000E goto 0x0004
                        0x280F, // 0x000F goto 0x000F
        };

        public static final char[] TPicSim15 = {
                        0x2810, // 0x0000 goto 0x0010
                        0x1683, // 0x0001 bsf 0x03,0x05
                        0x300E, // 0x0002 movlw 0x0E
                        0x0085, // 0x0003 movwf 0x05
                        0x300F, // 0x0004 movlw 0x0F
                        0x0086, // 0x0005 movwf 0x06
                        0x1283, // 0x0006 bcf 0x03,0x05
                        0x0008, // 0x0007 return
                        0x1683, // 0x0008 bsf 0x03,0x05
                        0x0085, // 0x0009 movwf 0x05
                        0x1283, // 0x000A bcf 0x03,0x05
                        0x0008, // 0x000B return
                        0x1683, // 0x000C bsf 0x03,0x05
                        0x0086, // 0x000D movwf 0x06
                        0x1283, // 0x000E bcf 0x03,0x05
                        0x0008, // 0x000F return
                        0x2001, // 0x0010 call 0x0001
                        0x1405, // 0x0011 bsf 0x05,0x00
                        0x0000, // 0x0012 nop
                        0x1005, // 0x0013 bcf 0x05,0x00
                        0x0000, // 0x0014 nop
                        0x1386, // 0x0015 bcf 0x06,0x07
                        0x0000, // 0x0016 nop
                        0x1786, // 0x0017 bsf 0x06,0x07
                        0x0000, // 0x0018 nop
                        0x300F, // 0x0019 movlw 0x0F
                        0x2008, // 0x001A call 0x0008
                        0x0000, // 0x001B nop
                        0x0000, // 0x001C nop
                        0x0000, // 0x001D nop
                        0x1405, // 0x001E bsf 0x05,0x00
                        0x0000, // 0x001F nop
                        0x0000, // 0x0020 nop
                        0x0000, // 0x0021 nop
                        0x300E, // 0x0022 movlw 0x0E
                        0x2008, // 0x0023 call 0x0008
                        0x0000, // 0x0024 nop
                        0x0000, // 0x0025 nop
                        0x0000, // 0x0026 nop
                        0x308F, // 0x0027 movlw 0x8F
                        0x200C, // 0x0028 call 0x000C
                        0x0000, // 0x0029 nop
                        0x0000, // 0x002A nop
                        0x1386, // 0x002B bcf 0x06,0x07
                        0x0000, // 0x002C nop
                        0x0000, // 0x002D nop
                        0x300F, // 0x002E movlw 0x0F
                        0x200C, // 0x002F call 0x000C
                        0x0000, // 0x0030 nop
                        0x0000, // 0x0031 nop
                        0x2832, // 0x0032 goto 0x0032
        };

        public static char[] TPicSim21 = {
                        0x0100, // 0x0000 clrw
                        0x2004, // 0x0001 call 0x0004
                        0x3CC8, // 0x0002 sublw 0xC8
                        0x2801, // 0x0003 goto 0x0001
                        0x2008, // 0x0004 call 0x0008
                        0x3E08, // 0x0005 addlw 0x08
                        0x00A0, // 0x0006 movwf 0x20
                        0x34F0, // 0x0007 retlw 0xF0
                        0x200B, // 0x0008 call 0x000B
                        0x3E07, // 0x0009 addlw 0x07
                        0x0008, // 0x000A return
                        0x200E, // 0x000B call 0x000E
                        0x3E06, // 0x000C addlw 0x06
                        0x0008, // 0x000D return
                        0x2011, // 0x000E call 0x0011
                        0x3E05, // 0x000F addlw 0x05
                        0x0008, // 0x0010 return
                        0x2014, // 0x0011 call 0x0014
                        0x3E04, // 0x0012 addlw 0x04
                        0x0008, // 0x0013 return
                        0x2017, // 0x0014 call 0x0017
                        0x3E03, // 0x0015 addlw 0x03
                        0x0008, // 0x0016 return
                        0x201A, // 0x0017 call 0x001A
                        0x3E02, // 0x0018 addlw 0x02
                        0x0008, // 0x0019 return
                        0x3800, // 0x001A iorlw 0x00
                        0x1D03, // 0x001B btfss 0x03,0x02
                        0x201F, // 0x001C call 0x001F
                        0x3E01, // 0x001D addlw 0x01
                        0x0008, // 0x001E return
                        0x3005, // 0x001F movlw 0x05
                        0x0008, // 0x0020 return
                        0x2821, // 0x0021 goto 0x0021
        };

        public static char[] TPicSim101 = {
                        0x3000, // 0x0000 movlw 0x00
                        0x008F, // 0x0001 movwf 0x0F
                        0x3005, // 0x0002 movlw 0x05
                        0x008E, // 0x0003 movwf 0x0E
                        0x3010, // 0x0004 movlw 0x10
                        0x0084, // 0x0005 movwf 0x04
                        0x3001, // 0x0006 movlw 0x01
                        0x008A, // 0x0007 movwf 0x0A
                        0x080F, // 0x0008 movf 0x0F,w
                        0x2109, // 0x0009 call 0x0109
                        0x0080, // 0x000A movwf 0x00
                        0x0A84, // 0x000B incf 0x04
                        0x0A8F, // 0x000C incf 0x0F
                        0x0B8E, // 0x000D decfsz 0x0E
                        0x2806, // 0x000E goto 0x0006
                        0x018A, // 0x000F clrf 0x0A
                        0x080F, // 0x0010 movf 0x0F,w
                        0x2109, // 0x0011 call 0x0109
                        0x2812, // 0x0012 goto 0x0012
                        0x0000, // 0x0013 nop
                        0x0000, // 0x0014 nop
                        0x0000, // 0x0015 nop
                        0x0000, // 0x0016 nop
                        0x0000, // 0x0017 nop
                        0x0000, // 0x0018 nop
                        0x0000, // 0x0019 nop
                        0x0000, // 0x001A nop
                        0x0000, // 0x001B nop
                        0x0000, // 0x001C nop
                        0x0000, // 0x001D nop
                        0x0000, // 0x001E nop
                        0x0000, // 0x001F nop
                        0x0000, // 0x0020 nop
                        0x0000, // 0x0021 nop
                        0x0000, // 0x0022 nop
                        0x0000, // 0x0023 nop
                        0x0000, // 0x0024 nop
                        0x0000, // 0x0025 nop
                        0x0000, // 0x0026 nop
                        0x0000, // 0x0027 nop
                        0x0000, // 0x0028 nop
                        0x0000, // 0x0029 nop
                        0x0000, // 0x002A nop
                        0x0000, // 0x002B nop
                        0x0000, // 0x002C nop
                        0x0000, // 0x002D nop
                        0x0000, // 0x002E nop
                        0x0000, // 0x002F nop
                        0x0000, // 0x0030 nop
                        0x0000, // 0x0031 nop
                        0x0000, // 0x0032 nop
                        0x0000, // 0x0033 nop
                        0x0000, // 0x0034 nop
                        0x0000, // 0x0035 nop
                        0x0000, // 0x0036 nop
                        0x0000, // 0x0037 nop
                        0x0000, // 0x0038 nop
                        0x0000, // 0x0039 nop
                        0x0000, // 0x003A nop
                        0x0000, // 0x003B nop
                        0x0000, // 0x003C nop
                        0x0000, // 0x003D nop
                        0x0000, // 0x003E nop
                        0x0000, // 0x003F nop
                        0x0000, // 0x0040 nop
                        0x0000, // 0x0041 nop
                        0x0000, // 0x0042 nop
                        0x0000, // 0x0043 nop
                        0x0000, // 0x0044 nop
                        0x0000, // 0x0045 nop
                        0x0000, // 0x0046 nop
                        0x0000, // 0x0047 nop
                        0x0000, // 0x0048 nop
                        0x0000, // 0x0049 nop
                        0x0000, // 0x004A nop
                        0x0000, // 0x004B nop
                        0x0000, // 0x004C nop
                        0x0000, // 0x004D nop
                        0x0000, // 0x004E nop
                        0x0000, // 0x004F nop
                        0x0000, // 0x0050 nop
                        0x0000, // 0x0051 nop
                        0x0000, // 0x0052 nop
                        0x0000, // 0x0053 nop
                        0x0000, // 0x0054 nop
                        0x0000, // 0x0055 nop
                        0x0000, // 0x0056 nop
                        0x0000, // 0x0057 nop
                        0x0000, // 0x0058 nop
                        0x0000, // 0x0059 nop
                        0x0000, // 0x005A nop
                        0x0000, // 0x005B nop
                        0x0000, // 0x005C nop
                        0x0000, // 0x005D nop
                        0x0000, // 0x005E nop
                        0x0000, // 0x005F nop
                        0x0000, // 0x0060 nop
                        0x0000, // 0x0061 nop
                        0x0000, // 0x0062 nop
                        0x0000, // 0x0063 nop
                        0x0000, // 0x0064 nop
                        0x0000, // 0x0065 nop
                        0x0000, // 0x0066 nop
                        0x0000, // 0x0067 nop
                        0x0000, // 0x0068 nop
                        0x0000, // 0x0069 nop
                        0x0000, // 0x006A nop
                        0x0000, // 0x006B nop
                        0x0000, // 0x006C nop
                        0x0000, // 0x006D nop
                        0x0000, // 0x006E nop
                        0x0000, // 0x006F nop
                        0x0000, // 0x0070 nop
                        0x0000, // 0x0071 nop
                        0x0000, // 0x0072 nop
                        0x0000, // 0x0073 nop
                        0x0000, // 0x0074 nop
                        0x0000, // 0x0075 nop
                        0x0000, // 0x0076 nop
                        0x0000, // 0x0077 nop
                        0x0000, // 0x0078 nop
                        0x0000, // 0x0079 nop
                        0x0000, // 0x007A nop
                        0x0000, // 0x007B nop
                        0x0000, // 0x007C nop
                        0x0000, // 0x007D nop
                        0x0000, // 0x007E nop
                        0x0000, // 0x007F nop
                        0x0000, // 0x0080 nop
                        0x0000, // 0x0081 nop
                        0x0000, // 0x0082 nop
                        0x0000, // 0x0083 nop
                        0x0000, // 0x0084 nop
                        0x0000, // 0x0085 nop
                        0x0000, // 0x0086 nop
                        0x0000, // 0x0087 nop
                        0x0000, // 0x0088 nop
                        0x0000, // 0x0089 nop
                        0x0000, // 0x008A nop
                        0x0000, // 0x008B nop
                        0x0000, // 0x008C nop
                        0x0000, // 0x008D nop
                        0x0000, // 0x008E nop
                        0x0000, // 0x008F nop
                        0x0000, // 0x0090 nop
                        0x0000, // 0x0091 nop
                        0x0000, // 0x0092 nop
                        0x0000, // 0x0093 nop
                        0x0000, // 0x0094 nop
                        0x0000, // 0x0095 nop
                        0x0000, // 0x0096 nop
                        0x0000, // 0x0097 nop
                        0x0000, // 0x0098 nop
                        0x0000, // 0x0099 nop
                        0x0000, // 0x009A nop
                        0x0000, // 0x009B nop
                        0x0000, // 0x009C nop
                        0x0000, // 0x009D nop
                        0x0000, // 0x009E nop
                        0x0000, // 0x009F nop
                        0x0000, // 0x00A0 nop
                        0x0000, // 0x00A1 nop
                        0x0000, // 0x00A2 nop
                        0x0000, // 0x00A3 nop
                        0x0000, // 0x00A4 nop
                        0x0000, // 0x00A5 nop
                        0x0000, // 0x00A6 nop
                        0x0000, // 0x00A7 nop
                        0x0000, // 0x00A8 nop
                        0x0000, // 0x00A9 nop
                        0x0000, // 0x00AA nop
                        0x0000, // 0x00AB nop
                        0x0000, // 0x00AC nop
                        0x0000, // 0x00AD nop
                        0x0000, // 0x00AE nop
                        0x0000, // 0x00AF nop
                        0x0000, // 0x00B0 nop
                        0x0000, // 0x00B1 nop
                        0x0000, // 0x00B2 nop
                        0x0000, // 0x00B3 nop
                        0x0000, // 0x00B4 nop
                        0x0000, // 0x00B5 nop
                        0x0000, // 0x00B6 nop
                        0x0000, // 0x00B7 nop
                        0x0000, // 0x00B8 nop
                        0x0000, // 0x00B9 nop
                        0x0000, // 0x00BA nop
                        0x0000, // 0x00BB nop
                        0x0000, // 0x00BC nop
                        0x0000, // 0x00BD nop
                        0x0000, // 0x00BE nop
                        0x0000, // 0x00BF nop
                        0x0000, // 0x00C0 nop
                        0x0000, // 0x00C1 nop
                        0x0000, // 0x00C2 nop
                        0x0000, // 0x00C3 nop
                        0x0000, // 0x00C4 nop
                        0x0000, // 0x00C5 nop
                        0x0000, // 0x00C6 nop
                        0x0000, // 0x00C7 nop
                        0x0000, // 0x00C8 nop
                        0x0000, // 0x00C9 nop
                        0x0000, // 0x00CA nop
                        0x0000, // 0x00CB nop
                        0x0000, // 0x00CC nop
                        0x0000, // 0x00CD nop
                        0x0000, // 0x00CE nop
                        0x0000, // 0x00CF nop
                        0x0000, // 0x00D0 nop
                        0x0000, // 0x00D1 nop
                        0x0000, // 0x00D2 nop
                        0x0000, // 0x00D3 nop
                        0x0000, // 0x00D4 nop
                        0x0000, // 0x00D5 nop
                        0x0000, // 0x00D6 nop
                        0x0000, // 0x00D7 nop
                        0x0000, // 0x00D8 nop
                        0x0000, // 0x00D9 nop
                        0x0000, // 0x00DA nop
                        0x0000, // 0x00DB nop
                        0x0000, // 0x00DC nop
                        0x0000, // 0x00DD nop
                        0x0000, // 0x00DE nop
                        0x0000, // 0x00DF nop
                        0x0000, // 0x00E0 nop
                        0x0000, // 0x00E1 nop
                        0x0000, // 0x00E2 nop
                        0x0000, // 0x00E3 nop
                        0x0000, // 0x00E4 nop
                        0x0000, // 0x00E5 nop
                        0x0000, // 0x00E6 nop
                        0x0000, // 0x00E7 nop
                        0x0000, // 0x00E8 nop
                        0x0000, // 0x00E9 nop
                        0x0000, // 0x00EA nop
                        0x0000, // 0x00EB nop
                        0x0000, // 0x00EC nop
                        0x0000, // 0x00ED nop
                        0x0000, // 0x00EE nop
                        0x0000, // 0x00EF nop
                        0x0000, // 0x00F0 nop
                        0x0000, // 0x00F1 nop
                        0x0000, // 0x00F2 nop
                        0x0000, // 0x00F3 nop
                        0x0000, // 0x00F4 nop
                        0x0000, // 0x00F5 nop
                        0x0000, // 0x00F6 nop
                        0x0000, // 0x00F7 nop
                        0x0000, // 0x00F8 nop
                        0x0000, // 0x00F9 nop
                        0x0000, // 0x00FA nop
                        0x0000, // 0x00FB nop
                        0x0000, // 0x00FC nop
                        0x0000, // 0x00FD nop
                        0x0000, // 0x00FE nop
                        0x0000, // 0x00FF nop
                        0x0000, // 0x0100 nop
                        0x0000, // 0x0101 nop
                        0x0000, // 0x0102 nop
                        0x0000, // 0x0103 nop
                        0x0000, // 0x0104 nop
                        0x0000, // 0x0105 nop
                        0x0000, // 0x0106 nop
                        0x0000, // 0x0107 nop
                        0x0000, // 0x0108 nop
                        0x0782, // 0x0109 addwf 0x02
                        0x3460, // 0x010A retlw 0x60
                        0x3461, // 0x010B retlw 0x61
                        0x3462, // 0x010C retlw 0x62
                        0x3463, // 0x010D retlw 0x63
                        0x3464, // 0x010E retlw 0x64
                        0x3465, // 0x010F retlw 0x65
                        0x3466, // 0x0110 retlw 0x66
                        0x3467, // 0x0111 retlw 0x67
                        0x3468, // 0x0112 retlw 0x68
                        0x3469, // 0x0113 retlw 0x69
                        0x346A, // 0x0114 retlw 0x6A
        };

}
