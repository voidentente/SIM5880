package SIM5880.backend;

/**
 * Defines the instruction set.
 */
public interface Instructions {

    ////////////////////////////////////////////////
    //// BYTE-ORIENTED FILE REGISTER OPERATIONS ////
    ////////////////////////////////////////////////

    /**
     * Add W and f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_0111_dfff_ffff
     */
    public void ADDWF();

    /**
     * AND W with f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_0101_dfff_ffff
     */
    public void ANDWF();

    /**
     * Clear f.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_0001_1fff_ffff
     */
    public void CLRF();

    /**
     * Clear W.
     * 
     * @opcode 00_0001_0xxx_xxxx
     */
    public void CLRW();

    /**
     * Complement f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_1001_dfff_ffff
     */
    public void COMF();

    /**
     * Decrement f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_0011_dfff_ffff
     */
    public void DECF();

    /**
     * Decrement f, Skip if 0.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @note If program counter (pc) is modified or a conditional test
     *       is true, the instruction requires two cycles.
     *       The second cycle is executed as a NOP.
     * 
     * @opcode 00_1011_dfff_ffff
     */
    public void DECFSZ();

    /**
     * Increment f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_1010_dfff_ffff
     */
    public void INCF();

    /**
     * Increment f, Skip if 0.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @note If program counter (pc) is modified or a conditional test
     *       is true, the instruction requires two cycles.
     *       The second cycle is executed as a NOP.
     * 
     * @opcode 00_1111_dfff_ffff
     */
    public void INCFSZ();

    /**
     * Inclusive OR W with f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_0100_dfff_ffff
     */
    public void IORWF();

    /**
     * Move f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_1000_dfff_ffff
     */
    public void MOVF();

    /**
     * Move W to f.
     * 
     * @opcode 00_0000_1fff_ffff
     */
    public void MOVWF();

    /**
     * No operation.
     * 
     * @opcode 00_0000_0xx0_0000
     */
    public void NOP();

    /**
     * Rotate left f through carry.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_1101_dfff_ffff
     */
    public void RLF();

    /**
     * Rotate right f through carry.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_1100_dfff_ffff
     */
    public void RRF();

    /**
     * Subtract W from f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_0010_dfff_ffff
     */
    public void SUBWF();

    /**
     * Swap nibbles in f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_1110_dfff_ffff
     */
    public void SWAPF();

    /**
     * Exclusive OR W with f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 00_0110_dfff_ffff
     */
    public void XORWF();

    /////////////////////////////////////////////////
    ///// BIT-ORIENTED FILE REGISTER OPERATIONS /////
    /////////////////////////////////////////////////

    /**
     * Bit clear f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 01_00bb_bfff_ffff
     */
    public void BCF();

    /**
     * Bit set f.
     * 
     * @note When an I/O register is modified as a function of itself
     *       (e.g. MOVF PORTB, 1), the value used will be that value
     *       present on the pins themselves. For example, if the data
     *       latch is '1' for a pin configured as input and is driven
     *       low bby an external device, the data will be written back
     *       with a '0'.
     * 
     * @note If this instruction is executed on the TMR0 register
     *       (and, where applicable, d = 1), the prescaler will be
     *       cleared if assigned to the Timer0 Module.
     * 
     * @opcode 01_01bb_bfff_ffff
     */
    public void BSF();

    /**
     * Bit test f, skip if clear.
     * 
     * @note If program counter (pc) is modified or a conditional test
     *       is true, the instruction requires two cycles.
     *       The second cycle is executed as a NOP.
     * 
     * @opcode 01_10bb_bfff_ffff
     */
    public void BTFSC();

    /**
     * Bit test f, skip if set.
     * 
     * @note If program counter (pc) is modified or a conditional test
     *       is true, the instruction requires two cycles.
     *       The second cycle is executed as a NOP.
     * 
     * @opcode 01_11bb_bfff_ffff
     */
    public void BTFSS();

    ////////////////////////////////////////////////
    //////// LITERAL AND CONTROL OPERATIONS ////////
    ////////////////////////////////////////////////

    /**
     * Add literal and W.
     * 
     * @opcode 11_111x_kkkk_kkkk
     */
    public void ADDLW();

    /**
     * AND literal with W.
     * 
     * @opcode 11_1001_kkkk_kkkk
     */
    public void ANDLW();

    /**
     * Call subroutine.
     * 
     * @opcode 10_0kkk_kkkk_kkkk
     */
    public void CALL();

    /**
     * Clear watchdog timer.
     * 
     * @opcode 00_0000_0110_0100
     */
    public void CLRWDT();

    /**
     * Go to address.
     * 
     * @opcode 10_1kkk_kkkk_kkkk
     */
    public void GOTO();

    /**
     * Inclusive OR literal with W.
     * 
     * @opcode 11_1000_kkkk_kkkk
     */
    public void IORLW();

    /**
     * Move literal to W.
     * 
     * @opcode 11_00xx_kkkk_kkkk
     */
    public void MOVLW();

    /**
     * Return from interrupt.
     * 
     * @opcode 00_0000_0000_1001
     */
    public void RETFIE();

    /**
     * Return with literal in W.
     * 
     * @opcode 11_01xx_kkkk_kkkk
     */
    public void RETLW();

    /**
     * Return from subroutine.
     * 
     * @opcode 00_0000_0000_1000
     */
    public void RETURN();

    /**
     * Go into standby mode.
     * 
     * @opcode 00_0000_0110_0011
     */
    public void SLEEP();

    /**
     * Subtract W from literal.
     * 
     * @opcode 11_110x_kkkk_kkkk
     */
    public void SUBLW();

    /**
     * Exclusive OR literal with W.
     * 
     * @opcode 11_1010_kkkk_kkkk
     */
    public void XORLW();

}