package SIM5880.backend;

import java.util.Scanner;
import java.io.IOException;
import java.io.DataInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * Wraps {@link Machine} with functionality that
 * does not contribute to the simulation, such as
 * utility methods to load programs into memory.
 */
public class MachineHelper implements Runnable {

    public final Machine machine;

    public MachineHelper(Machine machine) {
        this.machine = machine;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'run'");
    }

    public String fmtProgram() {
        String buf = new String();

        for (int i = 0; i < this.machine.program.length; i++) {
            buf += String.format("0x%04X", i) + " " + String.format("0x%04X", this.machine.program[i]) + "\n";
        }

        return buf;
    }

    public String fmtFileRegister() {
        String buf = new String();

        for (int i = 0; i < this.machine.fileRegister.length; i++) {
            buf += String.format("0x%04X", i) + " " + String.format("0x%04X", this.machine.fileRegister[i]) + "\n";
        }

        return buf;
    }

    public String fmtDisk() {
        String buf = new String();

        for (int i = 0; i < this.machine.disk.length; i++) {
            buf += String.format("0x%04X", i) + " " + String.format("0x%04X", this.machine.disk[i]) + "\n";
        }

        return buf;
    }

    public void loadProgram(short[] program) throws IOException {
        short[] tmp = new short[this.machine.program.length];
        System.arraycopy(program, 0, tmp, 0, program.length);
        this.machine.program = tmp;
    }

    public void loadProgramFile(Path path) throws IOException {
        DataInputStream stream = new DataInputStream(Files.newInputStream(path, StandardOpenOption.READ));
        int length = stream.available() / 2 + stream.available() % 2;
        short program[] = new short[length];

        for (int i = 0; i < stream.available() / 2; i++) {
            program[i] = stream.readShort();
        }

        stream.close();

        this.loadProgram(program);
    }

    public void loadListing(String listing) {
        short[] tmp = new short[this.machine.program.length];

        Scanner scanner = new Scanner(listing);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String address = line.substring(0, 4);
            String word = line.substring(5, 9);

            try {
                tmp[Short.parseShort(address, 16)] = Short.parseShort(word, 16);
            } catch (NumberFormatException e) {
                continue;
            }
        }

        scanner.close();

        this.machine.program = tmp;
    }

    public void loadListingFile(Path path) throws IOException {
        this.loadListing(Files.readString(path));
    }

}
