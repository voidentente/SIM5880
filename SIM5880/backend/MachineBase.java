package SIM5880.backend;

/**
 * Defines hardware-like characteristics,
 * such as stores, pins, and registers.
 */
public abstract class MachineBase {

    //////////////////////////////////////////
    ///////////////// STORES /////////////////
    //////////////////////////////////////////

    /**
     * Non-volatile program memory.
     * No distinction between Flash, EEPROM and ROM is made.
     */
    public short[] program;

    /**
     * Volatile data memory.
     * 
     * Split into two banks: SFR (registers) and GPR (variables).
     */
    public byte[] fileRegister;

    /**
     * Non-volatile data memory.
     */
    public byte[] disk;

    //////////////////////////////////////////
    ////////////////// OTHER /////////////////
    //////////////////////////////////////////

    /**
     * The working register.
     */
    protected byte workingRegister;

    protected void loadNextInstruction() {
        short address = (short) this.programCounter;
        address += ((short) this.programCounterLatch) << 8;
        this.instructionRegister = this.program[address];
        this.programCounter++;
    }

    /**
     * The instruction register contains the current instruction.
     */
    protected short instructionRegister;

    /**
     * The program counter.
     */
    protected byte programCounter;

    /**
     * The program counter latch.
     */
    protected byte programCounterLatch;

    //////////////////////////////////////////
    ///////////////// PORTS //////////////////
    //////////////////////////////////////////

    /**
     * Port A is a bi-directional I/O port.
     * 
     * @name RA2
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P01;

    /**
     * Port A is a bi-directional I/O port.
     * 
     * @name RA3
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P02;

    /**
     * Port A is a bi-directional I/O port.
     * 
     * Can also be selected to be the clock input to the TMR0 timer/counter.
     * Output is open drain type.
     * 
     * @name RA4 / T0CKI
     * @type Input/Output
     * @buffer Schmitt Trigger input
     */
    public boolean P03;

    /**
     * Master clear (reset) input/programming voltage input.
     * This pin is an active low reset to the device.
     * 
     * @name MCLR
     * @type Input/Power
     * @buffer Schmitt Trigger input
     */
    public boolean P04;

    /**
     * Ground reference for logic and Input/Output pins.
     * 
     * @name Vss
     * @type Power
     * @buffer Not used
     */
    public boolean P05;

    /**
     * Port B is a bi-directional I/O port.
     * Port B can be software programmed for internal weak pull-up on all inputs.
     * 
     * Can also be selected as an external interrupt pin.
     * 
     * This buffer is a Schmitt Trigger input when
     * configured as the external interrupt.
     * 
     * @name RB0 / INT
     * @type Input/Output
     * @buffer TTL input / Schmitt Trigger input
     */
    public boolean P06;

    /**
     * Port B is a bi-directional I/O port.
     * Port B can be software programmed for internal weak pull-up on all inputs.
     * 
     * @name RB1
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P07;

    /**
     * Port B is a bi-directional I/O port.
     * Port B can be software-programmed for internal weak pull-up on all inputs.
     * 
     * @name RB2
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P08;

    /**
     * Port B is a bi-directional I/O port.
     * Port B can be software-programmed for internal weak pull-up on all inputs.
     * 
     * @name RB3
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P09;

    /**
     * Port B is a bi-directional I/O port.
     * Port B can be software-programmed for internal weak pull-up on all inputs.
     * 
     * Interrupt on change pin.
     * 
     * @name RB4
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P10;

    /**
     * Port B is a bi-directional I/O port.
     * Port B can be software-programmed for internal weak pull-up on all inputs.
     * 
     * Interrupt on change pin.
     * 
     * @name RB5
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P11;

    /**
     * Port B is a bi-directional I/O port.
     * Port B can be software-programmed for internal weak pull-up on all inputs.
     * 
     * Interrupt on change pin.
     * Serial programming clock.
     * 
     * This buffer is a Schmitt Trigger input when
     * used in serial programming mode.
     * 
     * @name RB6
     * @type Input/Output
     * @buffer TTL input / Schmitt Trigger input
     */
    public boolean P12;

    /**
     * Port B is a bi-directional I/O port.
     * Port B can be software-programmed for internal weak pull-up on all inputs.
     * 
     * Interrupt on change pin.
     * Serial programming data.
     * 
     * This buffer is a Schmitt Trigger input when
     * used in serial programming mode.
     * 
     * @name RB7
     * @type Input/Output
     * @buffer TTL input / Schmitt Trigger input
     */
    public boolean P13;

    /**
     * Positive supply for logic and I/O pins.
     * 
     * @name Vdd
     * @type Power
     * @buffer Not used
     */
    public boolean P14;

    /**
     * Oscillator crystal output.
     * Connects to crystal or resonator in crystal oscillator mode.
     * In RC mode, OSC2 pin outputs CLKOUT which has 1/4 the frequency of OSC1,
     * and denotes the instruction cycle rate.
     * 
     * @name OSC2 / CLKOUT
     * @type Output
     * @buffer Not used
     */
    public boolean P15;

    /**
     * Oscillator crystal input/external clock source input.
     * 
     * This buffer is a Schmitt Trigger input when configured
     * in RC oscillator mode and a CMOS input otherwise.
     * 
     * @name OSC1 / CLKIN
     * @type Input
     * @buffer Schmitt Trigger input / CMOS
     */
    public boolean P16;

    /**
     * Port A is a bi-directional I/O port.
     * 
     * @name RA0
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P17;

    /**
     * Port A is a bi-directional I/O port.
     * 
     * @name RA1
     * @type Input/Output
     * @buffer TTL input
     */
    public boolean P18;

}
