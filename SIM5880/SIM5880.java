package SIM5880;

import java.time.Duration;
import java.awt.GraphicsEnvironment;

import SIM5880.frontend.NativeFrontend;

public class SIM5880 {

    public static void main(String[] args) throws InterruptedException {

        long nspt = 1_000_000_000
                / GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode()
                        .getRefreshRate();
        long last = System.nanoTime();
        long now, elapsed;

        int status = NativeFrontend.instance.init();

        while (NativeFrontend.STATUS_CONTINUE == status) {
            status = NativeFrontend.instance.update();

            now = System.nanoTime();
            elapsed = now - last;

            Thread.sleep(Duration.ofNanos(Math.max(nspt - elapsed, 0)));

            last = now;
        }

        NativeFrontend.instance.reset();

        Runtime.getRuntime().exit(status);

    }

}
