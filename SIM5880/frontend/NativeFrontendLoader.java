package SIM5880.frontend;

import java.io.IOException;

public abstract class NativeFrontendLoader {

    static {
        String target = System.getProperty("os.arch") + System.getProperty("os.name");

        switch (target) {
            case "amd64Windows":
                load("/SIM5880-rs/x86_64-pc-windows-gnu/SIM5880_rs.dll");
                break;
            case "amd64Linux":
                load("/SIM5880-rs/x86_64-unknown-linux-gnu/libSIM5880_rs.so");
                break;
            default:
                System.out.println("Natives not available for " + target);
                break;
        }
    }

    private static void load(String path) {
        try {
            NativeUtils.loadLibraryFromJar(path);
        } catch (IOException e) {
            e.printStackTrace();
            Runtime.getRuntime().exit(-1);
        }
    }

}
