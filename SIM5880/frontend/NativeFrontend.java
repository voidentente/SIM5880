package SIM5880.frontend;

import SIM5880.backend.MachineHelper;

/*
 * Frontend singleton that outsources
 * the user interface to the native library.
 * 
 * Contains all native methods and all 
 * callbacks that may be called by native code.
 */
public final class NativeFrontend extends NativeFrontendLoader {

    private MachineHelper machine;

    public static NativeFrontend instance = new NativeFrontend();

    private NativeFrontend() {
    }

    /*
     * Z - boolean
     * B - byte
     * C - char
     * S - short
     * I - int
     * J - long
     * F - float
     * D - double
     * L - Class
     * [type - type[]
     * (arg-types)ret-type - method type
     */

    public static final int STATUS_EXIT = 0;
    public static final int STATUS_CONTINUE = 1;
    public static final int STATUS_LOCKED = 2;
    public static final int STATUS_UNINITIALIZED = 3;
    public static final int STATUS_ERROR = 4;
    public static final int STATUS_PANIC = 5;

    public native int init();

    public native int update();

    public native int reset();

    public void cbSpawnMachine() {
        // new Thread(null, null);
    }

}
